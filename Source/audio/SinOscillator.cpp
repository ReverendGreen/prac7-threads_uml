//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Neil McGuiness on 25/10/2016.
//
//

#include <stdio.h>
#include <math.h>
#include "SinOscillator.h"



SinOscillator::SinOscillator()

{
    twoPi = M_PI * 2;
    phasePos = phaseInc = oscAmplitude = 0;
    
    //Setting initial sample rate to 44.1kHz as a default.
    sR = 44100.f;
    
}
SinOscillator::~SinOscillator()

{
    
}

float SinOscillator::tick()

{
    phasePos += phaseInc;
    
    if (phasePos > twoPi)
        
    {
        phasePos -= twoPi;
    }
    
    return sinf(phasePos) * oscAmplitude;
}
void SinOscillator::setSampleRate(const float sampleRate)

{
    sR = sampleRate;
}

void SinOscillator::setFreq (float frequency)

{
    phaseInc = twoPi * frequency / sR;
}

void SinOscillator::setAmp(float amp)

{
    oscAmplitude = amp;
}
