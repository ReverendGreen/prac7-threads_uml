//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Neil McGuiness on 25/10/2016.
//
//

#ifndef SinOscillator_h
#define SinOscillator_h


class SinOscillator

{
    
public:
    SinOscillator();
    
    ~SinOscillator();
    
    float tick();
    
    void setSampleRate(const float sampleRate);
    
    void setFreq (float frequency);
    
    void setAmp(float amp);
    
    
private:
    float  oscAmplitude, phasePos, phaseInc, sR, twoPi;
};


#endif /* SinOscillator_h */
