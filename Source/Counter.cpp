//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Neil McGuiness on 27/10/2016.
//
//

#include <stdio.h>
#include "Counter.h"



Counter::Counter() : Thread ("Counter Thread")

{
    counterValue = 0;
    interval = 60;
}


Counter::~Counter()

{
    
}

void Counter::run()
{
    while (!threadShouldExit())
        
    {
        
        
        uint32 time = Time::getMillisecondCounter();
        
        //Checking that a listener has been set in the compponent that inherits the Counter::Listener and is using (Has a) COunter.
        if (listener != nullptr)
            
        {
            // If it has, we call that classes counterChanged() function via this pointer and it will execute whatever code lies in that function.
            listener->counterChanged (counterValue++);
        }
        
        Time::waitForMillisecondCounter (time + interval);
    }
}


void Counter::setListener(Listener* newlistener)

{
    // Takes a pointer to a class that has inherited ("Is a") the Counter::Listener. and assigns our previously declared internal Listener* pointer to it.
    listener = newlistener;
}

void Counter::setInterval(float bpm)

{
    interval = 60000 / bpm;
}



//void Counter::stopThread()
//
//{
//    
//    Thread::stopThread(500);
//    
//}
