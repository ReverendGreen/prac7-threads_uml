/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
                                               

{
//    ampSlider.setRange(0.0f, 1.0f);
//    addAndMakeVisible(ampSlider);
//    ampSlider.addListener(this);
//    
    ampLabel.setText("Amplitude", dontSendNotification);
    ampLabel.attachToComponent(&metroAmpSlider, false);
    bpmLabel.setText("BPM", dontSendNotification);
    bpmLabel.attachToComponent(&bpmSlider, false);
    
    startStopButton.setButtonText("Start/Stop Counter");
    startStopButton.addListener(this);
    addAndMakeVisible(startStopButton);
    startStopButton.setToggleState(false, dontSendNotification);
    
    bpmSlider.setRange(60.f, 350.f);
    addAndMakeVisible(bpmSlider);
    bpmSlider.addListener(this);
    
    metroAmpSlider.setRange(0.0, 1.0);
    addAndMakeVisible(metroAmpSlider);
    metroAmpSlider.addListener(this);
    
    
    
    
    
    counterThread.setListener (this);
    
    setSize (500, 400);

}

MainComponent::~MainComponent()

{
    counterThread.stopThread(500);

}


void MainComponent::resized()

{
    metroAmpSlider.setBounds(30, 100, getWidth()/2, 30);
    bpmSlider.setBounds(30 , 200, getWidth()/2, 30);

    
    startStopButton.setBounds(30, 30, getWidth() / 5, 50);
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &startStopButton)
    {
        
        bool state = startStopButton.getToggleState();
        DBG (state);
        std::cout << state << std::endl;
        
        
        if (state)
        {
            counterThread.startThread();
        }
        else
        {
            counterThread.stopThread(500);
        }
    }

}

void MainComponent::sliderValueChanged (Slider* slider)

{
    if (slider == &bpmSlider)
        
    {
        counterThread.setInterval(bpmSlider.getValue());
    }
}






//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}


void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::counterChanged (const unsigned int counterValue)
{
    DBG("Count:" << (int)counterValue);
    audio.metro(metroAmpSlider.getValue());
}

